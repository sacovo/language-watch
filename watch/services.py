from datetime import timedelta
from typing import Dict

from django.db.models import F
from django.db.models.query import QuerySet
from django.utils import timezone

from watch.models import Event, Speaker


def speakers_as_dict(event: Event):
    speakers: QuerySet[Speaker] = event.speaker_set.annotate(
        duration=F("end") - F("start")
    )
    return [convert_speaker(speaker) for speaker in speakers]


def update_end_time(speaker: Speaker, end: str):
    hour, minute = end.split(":")
    hour, minute = int(hour), int(minute)

    naive = timezone.make_naive(speaker.end).replace(hour=hour, minute=minute)

    speaker.end = timezone.make_aware(naive)
    speaker.save()


def convert_speaker(speaker: Speaker) -> Dict:
    assert hasattr(speaker, "duration")

    if speaker.duration is None:
        duration: timedelta = timezone.now() - speaker.start
    else:
        duration: timedelta = speaker.duration

    return {
        "gender": speaker.gender,
        "language": speaker.language,
        "point": speaker.point,
        "position": speaker.position,
        "id": speaker.pk,
        "start": timezone.make_naive(speaker.start).strftime("%H:%M"),
        "end": timezone.make_naive(speaker.end).strftime("%H:%M")
        if speaker.end is not None
        else "",
        "duration": duration.seconds,
    }


def has_active_speaker(event: Event):
    return event.speaker_set.filter(end__isnull=True).exists()


def event_as_dict(event: Event):
    return {
        "pk": event.pk,
        "genders": [""] + event.genders,
        "languages": [""] + event.languages,
        "positions": [""] + event.positions,
        "points": [""] + [point.strip() for point in event.points.split("\n")],
    }


def read_fields(data, *fields):
    return {field: data.get(field, "") for field in fields}
