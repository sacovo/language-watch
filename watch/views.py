import uuid

from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.http.response import HttpResponseBadRequest, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.translation import gettext as _
from django.views.decorators.http import require_POST
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView

from watch.forms import EventForm
from watch.models import Event, Speaker
from watch.services import (
    event_as_dict,
    has_active_speaker,
    read_fields,
    speakers_as_dict,
    update_end_time,
)

# Create your views here.


class EventListView(LoginRequiredMixin, ListView):
    model = Event

    def get_queryset(self):
        return super().get_queryset().filter(owner=self.request.user)


class EventCreateView(LoginRequiredMixin, CreateView):
    model = Event
    form_class = EventForm

    success_url = reverse_lazy("watch:event_list")

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super(CreateView, self).form_valid(form)


class EventUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Event
    form_class = EventForm
    success_url = reverse_lazy("watch:event_list")

    def test_func(self):
        return self.get_object().owner == self.request.user


@require_POST
def start_speaker(request):
    pk = request.POST.get("pk", None) or uuid.UUID(int=0)

    event = get_object_or_404(Event, pk=pk)

    if event.speaker_set.filter(end__isnull=True).exists():
        return HttpResponseBadRequest(_("already started a speaker"))

    speaker = Speaker.objects.create(event=event)

    return JsonResponse(
        {
            "id": speaker.id,
        }
    )


@require_POST
def stop_speaker(request):
    pk = request.POST.get("pk", None) or uuid.UUID(int=0)

    event = get_object_or_404(Event, pk=pk)

    speaker = event.speaker_set.filter(end__isnull=True).first()

    if speaker is None:
        return HttpResponseBadRequest(_("no speaker is open"))

    speaker.end = timezone.now()
    speaker.save()

    return JsonResponse(
        {
            "id": speaker.id,
        }
    )


@require_POST
def update_speaker(request, pk):
    event = get_object_or_404(Event, pk=pk)

    speaker_pk = request.POST.get("pk", None)

    event.speaker_set.filter(pk=speaker_pk).update(
        **read_fields(request.POST, "gender", "language", "point", "position")
    )
    if end := request.POST.get("end"):
        update_end_time(event.speaker_set.filter(pk=speaker_pk).first(), end)

    event.send_update()

    return JsonResponse({"id": speaker_pk})


def delete_speaker(request, pk):
    event = get_object_or_404(Event, pk=pk)

    speaker_pk = request.POST.get("pk", None)
    event.speaker_set.filter(pk=speaker_pk).delete()

    event.send_update()

    return JsonResponse({"id": speaker_pk})


def event_json(request, pk):
    event = get_object_or_404(Event, pk=pk)

    return JsonResponse(
        {
            "speakers": speakers_as_dict(event),
            "isOpen": has_active_speaker(event),
        }
    )


class EventDetailView(DetailView):
    model = Event

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data["eventDict"] = event_as_dict(data["event"])
        return data


class EventStatsView(LoginRequiredMixin, UserPassesTestMixin, EventDetailView):
    template_name = "watch/event_stats.html"

    def test_func(self):
        return self.get_object().owner == self.request.user
