# Generated by Django 4.0.2 on 2022-02-15 21:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('watch', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='points',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='speaker',
            name='point',
            field=models.CharField(blank=True, max_length=255),
        ),
    ]
