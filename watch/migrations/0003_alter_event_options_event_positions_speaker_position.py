# Generated by Django 4.0.2 on 2022-02-16 09:40

from django.db import migrations, models

import watch.models


class Migration(migrations.Migration):

    dependencies = [
        ('watch', '0002_event_points_speaker_point'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='event',
            options={'ordering': ['-created']},
        ),
        migrations.AddField(
            model_name='event',
            name='positions',
            field=models.JSONField(default=watch.models.default_positions),
        ),
        migrations.AddField(
            model_name='speaker',
            name='position',
            field=models.CharField(blank=True, max_length=60),
        ),
    ]
