from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import gettext_lazy as _
from model_utils.models import TimeStampedModel, UUIDModel

channel_layer = get_channel_layer()


def default_genders():
    return [x.strip() for x in getattr(settings, 'DEFAULT_GENDERS', 'm, f, o').split(',')]


def default_languages():
    return [x.strip() for x in getattr(settings, 'DEFAULT_LANGUAGES', 'de,fr,it').split(',')]


def default_positions():
    return [x.strip() for x in getattr(settings, 'DEFAULT_POSITIONS', 'GL,Basis').split(',')]


class Event(TimeStampedModel, UUIDModel):
    name = models.CharField(max_length=255)
    owner = models.ForeignKey(get_user_model(), models.CASCADE)
    genders = models.JSONField(default=default_genders)
    languages = models.JSONField(default=default_languages)

    points = models.TextField(blank=True)
    positions = models.JSONField(default=default_positions)

    def send_update(self):
        print(f'event_{self.id.hex}')
        async_to_sync(channel_layer.group_send)(f'event_{self.id.hex}', {"type": "event.update"})

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        self.send_update()

    class Meta:
        ordering = ['-created']


class Speaker(models.Model):
    start = models.DateTimeField(verbose_name=_("start"), auto_now_add=True)
    end = models.DateTimeField(verbose_name=_("end"), null=True, blank=True)

    event = models.ForeignKey(Event, models.CASCADE)

    gender = models.CharField(max_length=50, blank=True)
    language = models.CharField(max_length=4, blank=True)
    position = models.CharField(max_length=60, blank=True)
    point = models.CharField(max_length=255, blank=True)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        print("Update")
        self.event.send_update()

    class Meta:
        ordering = ['-start']
