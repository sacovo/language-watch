from django import forms
from django.forms.widgets import TextInput

from watch.models import Event


class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = ["name", "genders", "languages", "positions", "points"]

        widgets = {
            "genders": TextInput,
            "languages": TextInput,
            "positions": TextInput,
        }
