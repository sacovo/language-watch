import { createApp } from "https://unpkg.com/vue@3.2.31/dist/vue.esm-browser.prod.js";

const eventData = JSON.parse(document.querySelector("#event-data").textContent);

let speakers = await fetch("../data/").then((response) =>
	response.json().then((json) => {
		return json["speakers"];
	}),
);

const app = createApp({
	data() {
		return {
			...eventData,
			speakers: undefined,
			groupers: [],
			filters: {},
		};
	},
	methods: {
		refresh() {
			fetch("../data/").then((response) =>
				response.json().then((json) => {
					this.speakers = new dfd.DataFrame(json["speakers"]);
					this.initPieChart();
				}),
			);
		},
		drawCharts() {
			this.drawPieChart();
		},
		initPieChart() {
			Plotly.newPlot("pieChartDiv", [
				{
					values: [1, 23, 4],
					labels: ["hallo"],
					type: "pie",
				},
				{},
				{
					responsive: true,
				},
			]);
			Plotly.newPlot("barChartDiv", [
				{
					x: ["a", "b", "c"],
					y: [10, 12, 11],
					type: "bar",
				},
				{},
				{
					response: true,
				},
			]);
		},
		drawPieChart() {
			const formData = new FormData(document.getElementById("pieChartForm"));
			let groups = formData.getAll("groupers");
			if (groups.length < 1) {
				return;
			}

			let durationEnabled = formData.get("durationEnabled");
			let q = ["gender", "language", "position", "point"]
				.map((attr) => {
					const filter = formData.get("filter_" + attr);
					if (!filter) {
						return this.speakers[attr].eq(this.speakers[attr]);
					}
					return this.speakers[attr].eq(filter);
				})
				.reduce((acc, cur) => (acc ? acc.and(cur) : cur));
			q.print();
			let df = this.speakers.query(q);

			let dfGrouped = df.groupby(groups).col(["duration"]);
			let labels = Object.keys(dfGrouped.groups);

			let values = labels
				.map((label) => dfGrouped.groups[label].duration)
				.map((durations) => {
					return durationEnabled
						? durations.reduce((acc, cur) => acc + cur, 0)
						: durations.length;
				});
			var data = [
				{
					values: values,
					labels: labels,
					type: "pie",
				},
			];

			Plotly.animate("pieChartDiv", {
				data: data,
			});
		},
	},
	mounted() {
		this.refresh();
	},
});

app.config.compilerOptions.delimiters = ["${", "}"];
app.mount("#app");
