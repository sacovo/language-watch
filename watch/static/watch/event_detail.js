import { createApp } from "https://unpkg.com/vue@3.2.31/dist/vue.esm-browser.prod.js";

const eventData = JSON.parse(document.querySelector("#event-data").textContent);

const csrftoken = document.querySelector("[name=csrfmiddlewaretoken]").value;
const formData = new FormData();

formData.append("pk", eventData.pk);

const app = createApp({
  data() {
    return {
      ...eventData,
      speakers: [],
      isOpen: false,
      lastUpdate: new Date(),
      wantDeletion: null,
    };
  },
  methods: {
    refresh() {
      fetch("data/").then((response) =>
        response.json().then((json) => {
          this.speakers = json["speakers"];
          this.isOpen = json["isOpen"];
        })
      );
    },
    start() {
      fetch("/start/", {
        method: "POST",
        headers: {
          "X-CSRFToken": csrftoken,
        },
        body: formData,
        mode: "same-origin",
      });
    },
    stop() {
      fetch("/stop/", {
        method: "POST",
        headers: {
          "X-CSRFToken": csrftoken,
        },
        body: formData,
        mode: "same-origin",
      });
    },
    onSpeakerChange(id) {
      fetch(`speaker/update/`, {
        method: "POST",
        headers: {
          "X-CSRFToken": csrftoken,
        },
        body: new FormData(document.querySelector(`#form_${id}`)),
        mode: "same-origin",
      });
    },
    initiateDelete(id) {
      this.wantDeletion = id;
    },
    cancelDelete() {
      this.wantDeletion = null;
    },
    deleteSpeaker(event) {
      event.preventDefault();

      fetch(`speaker/delete/`, {
        method: "POST",
        headers: {
          "X-CSRFToken": csrftoken,
        },
        body: new FormData(event.target),
        mode: "same-origin",
      });
    },
  },
  computed: {
    countRequests() {
      return this.speakers.length;
    },
    totalDuration() {
      const totalSeconds = this.speakers.reduce(
        (acc, cur) => acc + cur.duration,
        0
      );
      const hours = String(Math.floor(totalSeconds / 3600)).padStart(2, "0");
      const minutes = String(Math.floor((totalSeconds % 3600) / 60)).padStart(
        2,
        "0"
      );
      const seconds = String(Math.floor(totalSeconds % 60)).padStart(2, "0");

      return `${hours}:${minutes}:${seconds}`;
    },
  },
  mounted() {
    const domain = window.location.host;
    const protocol = window.location.protocol === "https:" ? "wss:" : "ws:";

    let socket = new WebSocket(
      `${protocol}//${domain}/ws/events/${eventData.pk}/`
    );
    socket.onmessage = () => this.refresh();
    this.refresh();

    setInterval(() => {
      let now = new Date();
      let delta = (now - this.lastUpdate) / 1000;
      if (this.speakers[0] && !this.speakers[0].end) {
        this.speakers[0].duration = Math.floor(
          this.speakers[0].duration + delta
        );
      }
      this.lastUpdate = now;
    }, 1000);
  },
});
app.config.compilerOptions.delimiters = ["${", "}"];
app.mount("#app");
