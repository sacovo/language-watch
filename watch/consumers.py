from channels.generic.websocket import AsyncWebsocketConsumer


class EventConsumer(AsyncWebsocketConsumer):

    async def connect(self):
        self.event_id = self.scope['url_route']['kwargs']['event_id']
        self.event_layer_name = f'event_{self.event_id.hex}'
        print(self.event_layer_name)

        await self.channel_layer.group_add(self.event_layer_name, self.channel_name)
        print(self.event_layer_name)

        await self.accept()

    async def disconnect(self, code):
        await self.channel_layer.group_discard(self.event_layer_name, self.channel_name)
        return await super().disconnect(code)

    async def event_update(self, event):
        await self.send(text_data="update")
