from django.urls import path

from watch.views import (
    EventCreateView,
    EventDetailView,
    EventListView,
    EventStatsView,
    EventUpdateView,
    delete_speaker,
    event_json,
    start_speaker,
    stop_speaker,
    update_speaker,
)

app_name = "watch"

urlpatterns = [
    path("", EventListView.as_view(), name="event_list"),
    path("create/", EventCreateView.as_view(), name="event_create"),
    path("start/", start_speaker, name="start_speaker"),
    path("stop/", stop_speaker, name="stop_speaker"),
    path("<uuid:pk>/speaker/update/", update_speaker, name="speaker_update"),
    path("<uuid:pk>/speaker/delete/", delete_speaker, name="speaker_delete"),
    path("<uuid:pk>/update/", EventUpdateView.as_view(), name="event_update"),
    path("<uuid:pk>/stats/", EventStatsView.as_view(), name="event_stats"),
    path("<uuid:pk>/data/", event_json, name="event_data"),
    path("<uuid:pk>/", EventDetailView.as_view(), name="event_detail"),
]
