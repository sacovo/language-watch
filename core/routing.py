from django.urls import path

from watch import consumers

websocket_urlpatterns = [
    path("ws/events/<uuid:event_id>/", consumers.EventConsumer.as_asgi()),
]
